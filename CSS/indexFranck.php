<!DOCTYPE>
<html>
  <head>
    <h1>Time Screen</h1><br>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css.css"/>
  </head>
<br>    
  <body>
   
    <?php
    $calendrier=file_get_contents('http://adeweb.univ-reunion.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=6928,6927,4776,4775,6888,4773,6487,4&projectId=4&calType=ical&nbWeeks=4');

      //Définition des expressions régulières qui vont permettre de récupérer les informations>
      $regExpSum = "/SUMMARY:(.*)/";
      $regExpLoc = "/LOCATION:(.*)/";
      $regExpDesc = "/DESCRIPTION:(.*)/";
      $regExpDateDebut = "/DTSTART:(.*)/";
      

      //Récupération des occurences trouvées dans le calendrier
      $n = preg_match_all($regExpSum,$calendrier,$sumTableau,PREG_PATTERN_ORDER);
      preg_match_all($regExpLoc,$calendrier,$locTableau,PREG_PATTERN_ORDER);
      preg_match_all($regExpDesc,$calendrier,$desTableau,PREG_PATTERN_ORDER);
      preg_match_all($regExpDateDebut,$calendrier,$dateDebutTableau,PREG_PATTERN_ORDER);
      

      //Création d'une boucle qui va permettre de lire le ou les tableaux générés, de récupérer les informations intéressantes et de les mettres en forme.
          
      for ($j=0;$j<$n;++$j){
        //Récupération des données
        $module = substr($sumTableau[0][$j],8);
        $salle = substr($locTableau[0][$j],9);
        $desc = substr($desTableau[0][$j],13);
        $mois = substr($dateDebutTableau[0][$j], 12, 2);
        $jour = substr($dateDebutTableau[0][$j], 14, 2);
        $heure = substr($dateDebutTableau[0][$j], 17, 2);
        $min = substr($dateDebutTableau[0][$j], 19, 2);
        $date = $jour.'/'.$mois;
        $horaire = $heure.'h'.$min;

        //Affichage
          
      
      ?>
       
      <table width=100%>
        <tr>
         <th>Module </th>   
         <th> Salle</th>    
         <th> Description</th>  
         <th> Date</th>
         <th> Horaire</th>    
         </tr>
  
        
      <td>
          <?php 
          Print("$module");
          ?>
          </td>
         <td>
          <?php 
          Print("$salle");
          ?>
          </td>   
             <td>
          <?php 
          Print("$desc");
          ?>
          </td> 
           
             <td>
          <?php 
          Print("$date");
          ?>
          </td> 
           
             <td>
          <?php 
          Print("$horaire");
          ?>
          </td> 
           
    <?php          
      echo "<br>";
      echo "<br>";    
      }
      ?>
      </table>
    </body>
    </html>
